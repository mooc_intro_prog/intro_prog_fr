import os
import nbformat
import subprocess
import json
import sys
from pathlib import  Path
from nbconvert.preprocessors import ExecutePreprocessor
import glob

def ascii_to_md(asciitxt):
    # save file to disk 
    f = open("tmp.adoc", "w")
    f.write(asciitxt)
    f.close()

    # convert file
    asciidoctor= subprocess.run(['asciidoctor', '-b','docbook', 'tmp.adoc'])
    pandoc = subprocess.run(['pandoc',  '-f',  'docbook' , '-t',  'gfm',  '-o' , 'tmp.md',  'tmp.xml'])
    # read new md file
    f = open("tmp.md", "r")
    md = f.read() 
    f.close()
    # delete tmp files
    os.remove("tmp.md")
    os.remove("tmp.adoc")
    os.remove("tmp.xml")
    return md

inputdir = sys.argv[1]

fileList = glob.glob(inputdir+"/*/*.asciidoc")

for file in fileList:
    nb = nbformat.v4.new_notebook()
    listcells =[]
    print("File to process {}".format(file))
    with open(file) as asciidoc:
        data = asciidoc.read()

    cells = data.split("+*")

    if(len(cells)>=1):
        for elem in cells:
            md =""
            if "In[" in elem:
                #print("Code cell : \n {}".format(elem.split("----")[1]))
                listcells.append(nbformat.v4.new_code_cell(elem.split("----")[1]))
            
            elif "Out[" in elem:
                #print("out cell : \n {}".format(elem))
                for item in elem.split("----"):
                    if item.startswith("\n\n") and item.endswith("\n\n\n"):
                        md += item

            else :
                md += elem

            md = ascii_to_md(md)
            if len(md)> 0 :
                listcells.append(nbformat.v4.new_markdown_cell(md))
                #print("Md cell : \n {}".format(md))

    nb['cells'] = listcells
    ep = ExecutePreprocessor(timeout=600, kernel_name='scala')
    ep.allow_errors=True
    ep.preprocess(nb)
    output = str(Path(file).parent)+"/"+Path(file).stem+".ipynb"
    nbformat.write(nb, output)
    print("Save to {}".format(output))