Introduction à la programmation avec Scala

- un dossier par module
- dans chaque module le contenu est dans un dossier avec son numéro de référence
- fichier .md a utiliser pour les quiz et énoncé de production de code
- fichier .ipynb  pour le contenue des notebooks
- les ppts de consultation vidéo sont des liens vers Google Drive qui host les fichiers
- tout les fichiers sont nommés par leur numéro de référence.

Quiz canvas :

## Titre

Description comme dans le plan du cours.

Seuil de réussite: 80%
Temps : 5 min
Fonction du quiz: Formative/Certificative

Q1 : Énoncé
Type de question: 
- Réponses correctes multiples
- Réponse correcte unique 
- Choix multiple de réflexion
- Choix unique de réflexion

Réponses :
Correct/Incorrect -> Feedback si sélectioné