## Cycle

Vous allez répondre à un questionnaire sur le cycle de la programmation

Seuil de réussite: 80%

Fonction du quiz: Formative

Q1 : Quel est la première étape dans le cycle de programmation ?
Type de question: 
- Réponse correcte unique 
Réponses :

- Analyse Correct  -> En effet, la première étage est toujours l'analyse du problème.

- Codage Incorrect -> Le codage est la seconde étape du cycle de programmation.

- Test -> Incorrect Les tests viennents après le codage.

Q2 : Que rédige le programmeur/ développeur ?

Type de question: 
- Réponse correcte unique 

Réponses :
- Le programme source en langage de haut niveau Correct -> En effet, il écrit du code source en langage de haut niveau et non du language machine (001011).
- Le programme source en langage machine  Incorrect -> Le langage machine est du langage binaire (01100), le développeur n'écrit pas le code source avec des 1 et des 0.
- Le programme exécutable en langage machine  Incorrect -> Le programmeur ne programme pas un exécutable.
- Le programme exécutable en langage de haut niveau Incorrect -> Le programmeur ne programme pas un exécutable.

Q3 : A quoi sert le compilateur ?

Type de question: 
- Réponse correcte unique 

Réponses :
- A traduire le code de haut niveau d’un programme en langage machine pour qu’il puisse être exécuté Correct -> En effet, le compilateur va traduire le code haut niveau en langage machine pour qu’il puisse être exécuté.
- A vérifier la syntaxe du code Incorrect -> Le compilateur ne vérifie pas le code source.
- A exécuter le programme sur l’ordinateur Incorrect -> Le compilateur n'exécute pas le programme.


Q4 : Que se passe-t-il s’il y a une erreur de syntaxe dans le code d’un programme ?

Type de question: 
- Réponse correcte unique 

Réponses :
- Il ne pourra pas être compilé Correct -> Le compilateur ne pourra pas compiler le programme.
- Il pourra être compilé mais pas exécuté Incorrect ->  Le compilateur ne pourra pas compiler le programme.
- Il pourra être compilé et exécuté, mais donnera une erreur Incorrect -> Le compilateur ne pourra pas compiler le programme. Donc aucun exécutable ne sera généré.


Q5 : Si vous modifiez le code d’un programme, il doit être :

Type de question: 
- Réponse correcte unique 

Réponses :
- Recompilé pour pouvoir l’exécuter avec la modification Correct -> Quand le code source change, il faut obligatoirement recompiler le programme.
- Décompilé pour pouvoir l’exécuter avec la modification Incorrect  -> Décompiler est l'action de prendre un programme exécutable et de le transformer en code source. Cette action est souvent effectuée par les chercheurs en sécurité informatique.
- Allumé pour pouvoir l’exécuter avec la modification Incorrect -> Allumer le code d'un programme n'a pas de sens.
