# YAML: reflective quiz questions not supported

import yaml
import re
import sys

quiz_question = sys.argv[1]
yaml_file = open(quiz_question, 'r', encoding="utf-8")
my_dict = yaml.safe_load(yaml_file)

# REWRITES QUIZZES PER TYPE
new_format = ""
for i in my_dict:
	try:
		quiz_type = i["variations"][0]["typeName"]
		question = i["variations"][0]["prompt"].replace("\n", " ").replace("&", "&amp;").replace("<img assetId=", "<img src=").replace(" />", " />").replace("  ", "<br/><br/>\n\n")

		# MULTIPLE CHOICE *********************************************************
		if quiz_type == "multipleChoice":
			new_format = new_format + ">><h2>" + question + "</h2><<\n\n"
			feedback = "" 
			for a in i["variations"][0]["options"]:
				status = a["isCorrect"]
				answer = a["answer"].replace("\n", " ").replace("&", "&amp;").replace("<img assetId=", "<img src=")
				if status == True: 
					new_format = new_format + "(!x) " + answer
					if a["feedback"] != '':
						new_format = new_format + " {{" + a["feedback"].replace("&", "&amp;").replace("\n", " ").replace("[", "&#91;").replace("]", "&#93;") + "}}" + "\n"
					else:
						new_format = new_format + "\n"
				else: 
					new_format = new_format + "(!) " + answer
					if a["feedback"] != '':
						new_format = new_format + " {{" + a["feedback"].replace("&", "&amp;").replace("\n", " ").replace("[", "&#91;").replace("]", "&#93;") + "}}" + "\n"
					else:
						new_format = new_format + "\n"
			if feedback != "": 
				new_format = new_format + "\n[explanation]\n<br/>"
				feedback = feedback + "[explanation]"
				new_format = new_format + feedback
			
		# CHECK BOX ***************************************************************
		elif quiz_type == "checkbox":
			new_format = new_format + ">><h2>" + question + "</h2><<\n\n"
			feedback = ""
			for a in i["variations"][0]["options"]:
				status = a["isCorrect"]
				answer = a["answer"].replace("\n", " ").replace("&", "&amp;").replace("<img assetId=", "<img src=")
				if status == True: 
					new_format = new_format + "[x] " + answer
					if a["feedback"] != '':
						new_format = new_format + " {{ selected: " + answer.replace("&", "&amp;").replace("[", "&#91;").replace("]", "&#93;") + ": <br/>Yes. " + a["feedback"].replace("&", "&amp;").replace("\n", " ").replace("[", "&#91;").replace("]", "&#93;") + "<br/><br/>}}" + "\n"
					else:
						new_format = new_format + "\n"
				else: 
					new_format = new_format + "[!] " + answer
					if a["feedback"] != '':
						new_format = new_format + " {{ selected: " + answer.replace("&", "&amp;").replace("[", "&#91;").replace("]", "&#93;") + ": <br/>No. " + a["feedback"].replace("&", "&amp;").replace("\n", " ").replace("[", "&#91;").replace("]", "&#93;") + "<br/><br/>}}" + "\n"
					else:
						new_format = new_format + "\n"
			if feedback != "":
				new_format = new_format + "\n[explanation]\n<br/>"
				feedback = feedback + "[explanation]"
				new_format = new_format + feedback
		
		# TEXT INPUT **************************************************************	
		elif quiz_type == "text": 
			new_format = new_format + ">>" + question + "<<\n\n"
			answers = []
			feedback = ""
			for a in i["variations"][0]["answers"]: 
				answers.append(a["answer"].replace("\n", " ").replace("&", "&amp;"))
				if a["feedback"] != '':
						feedback = feedback + a["answer"].replace("\n", " ").replace("&", "&amp;").replace("[", "&#91;").replace("]", "&#93;") + ": " + a["feedback"].replace("\n", " ").replace("[", "&#91;").replace("]", "&#93;") + "\n"
			for b in answers:
				if answers.index(b) == 0: 
					new_format = new_format + "= " + b + "\n"
				else:
					new_format = new_format + "or=" + b + "\n"
			if feedback != "":
				new_format = new_format + "\n[explanation]\n"
				new_format = new_format + "Incorrect answer: " + i["variations"][0]["defaultFeedback"].replace("\n", " ").replace("&", "&amp;").replace("[", "&#91;").replace("]", "&#93;") + "\n"
				feedback = feedback + "[explanation]"
				new_format = new_format + feedback
		

		# NUMERIC INPUT ***********************************************************
		elif quiz_type == "numeric": 
			new_format = new_format + ">>" + question + "<<\n\n"
			feedback = ""
			try:
				answer = i["variations"][0]["answers"][0]["value"]
			except:
				answer = i["variations"][0]["answers"][0]["range"]
			new_format = new_format + "= " + str(answer)
			if i["variations"][0]["defaultFeedback"] != '':
				feedback = feedback + i["variations"][0]["defaultFeedback"].replace("\n", " ").replace("&", "&amp;").replace("[", "&#91;").replace("]", "&#93;") + "\n"
			if feedback != "":
				new_format = new_format + "\n[explanation]\n<br/>"			
				feedback = feedback + "[explanation]"
				new_format = new_format + feedback


		# MATH EXPRESSIONS ********************************************************
		if quiz_type == "mathExpression": 
			answers = []
			feedback = ""
			for a in i["variations"][0]["answers"]: 
				answers.append(a["answer"].replace("\n", " ").replace("&", "&amp;"))
				if a["feedback"] != '':
					feedback = feedback + a["answer"].replace("\n", " ").replace("&", "&amp;").replace("[", "&#91;").replace("]", "&#93;") + ": " + a["feedback"].replace("\n", " ").replace("[", "&#91;").replace("]", "&#93;") + "\n"

			# building variables with training values
			for b in answers:
				if answers.index(b) == 0: 
					b2 = list(set(re.split("[()\+\-\*/^ ]", answers[0])))
					c = []
					for i in b2:
						if i == "asin":
							c.append("arcsin")
						else:
							d = "".join(re.findall("sin|cos|[0-9]+_[0-9]+|^[0-9]+[0-9]$|^[0-9]+.[0-9]$|^[0-9]+$", i))
							if i != d:
								c.append(i)
			
					ME_var1 = ",".join(c)
					ME_val1 = []
					for l in range(len(c)):
						ME_val1.append("0.1")
					ME_val2 = []
					for l in range(len(c)):
						ME_val2.append("10")

					if feedback != "": 
						ME_problem = """
<problem>
<formularesponse samples="{}@{}:{}#10" answer="{}">
  <label>{}</label>
<formulaequationinput/>
<solution>
<div class="detailed-solution">
<p>Explanation</p>
<p>{}</p>
</div>
</solution>
</formularesponse>
</problem>""".format(ME_var1, ",".join(ME_val1), ",".join(ME_val2), answers[0], question, feedback)
					else: 
						ME_problem = """
<problem>
<formularesponse samples="{}@{}:{}#10" answer="{}">
  <label>{}</label>
<formulaequationinput/>
</formularesponse>
</problem>""".format(ME_var1, ",".join(ME_val1), ",".join(ME_val2), answers[0], question)

					new_format = new_format + ME_problem


	except:
		new_format = new_format + "* * * * * * * * * * M I S S I N G * A * Q U E S T I O N * H E R E * * * * * * * * * *"
	
	new_format = new_format + "\n\n\n\n\n"


# ADDS RETURNS + REPLACES LATEX $$...$$ BY \(...\)
for i in new_format.split("\n"):
	i = re.sub("\$\$([^\$\$]*)\$\$", r"\\(\1\\)", i)
	print(i)


yaml_file.close()