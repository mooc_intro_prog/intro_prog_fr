{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebooks\n",
    "\n",
    "Notebooks are not computer development environments. In the context of this course, they are rather meant to be **educational environments** allowing to **illustrate and experiment each notion studied from interactive examples**.\n",
    "\n",
    "You will do the first exercises in the notebooks, but we will make a transition to the IDE (Integrated Development Environment) IntelliJ which is a real development environment. \n",
    "\n",
    "The installation on your computer and the writing and execution of a first program is presented in the lesson \"Installation of the IDE\" of the first module of the course.\n",
    "\n",
    "The two environments: notebooks and IntelliJ allow you to write, compile and execute code, but in different situations. \n",
    "\n",
    "**A notebook is an interactive document** in which you can *execute code, display the output, but also add explanations, formulas, graphics...*.\n",
    "\n",
    "It is composed of a **suite of cells**, a bit like in a spreadsheet of different types:\n",
    "- code for interactive examples (see next cell)\n",
    "- text for explanations (the cell that contains this explanation)\n",
    "\n",
    "Code cells allow you to **draw code fragments (a sequence of instructions of varying size), compile it, execute it and see the result of the code execution**. The behavior of code cells is similar to the way cells in a spreadsheet work.\n",
    "\n",
    "When a code cell is executed, the code it contains is sent to the Scala compiler. The results that are returned from the calculation that was performed are then displayed in the notebook as the cell's output. \n",
    "\n",
    "A code cell **consists of two parts**:\n",
    "- The *\"In \"* part which contains the code\n",
    "- The *\"Out \"* part which shows the result of the compilation and execution of the code\n",
    "\n",
    "If the code **contains compilation errors**, it cannot be executed and a message explaining the detected error will be displayed instead of the code result. You will then have to **correct the error and run the cell again**.\n",
    "\n",
    "In a notebook that contains several code cells, a cell will be able to access everything that has been declared (in particular variables/methods and classes) declared in the previous cells.\n",
    "\n",
    "## Tip\n",
    "\n",
    "Do not hesitate to modify the notebooks. If you have any questions: **\"what happens if ... ? \"**, you can test it by modifying the code of a cell or by adding a cell and running it. That's what this environment is for and you won't damage it. The more you try, the more you test, the better you will understand how the code works.\n",
    "\n",
    "## An example of a code cell\n",
    "\n",
    "The following cell is a code cell. It is marked with the label *\"Enter []\"*. Place the cursor in the cell and write: \n",
    "\n",
    "val message = \"I got it\"<br>\n",
    "println(message)\n",
    "\n",
    "then click on the *\"Run \"* button which is in the toolbar under the menus at the top of this notebook window. \n",
    "\n",
    "The cell will be numbered (the number, x, appears between the [] in the cell label: Enter[x]. Below the cell, you should see\n",
    "\n",
    "*I got it*\n",
    "\n",
    "And again below\n",
    "\n",
    "*message: String = \"I got it \"* preceded by the label Out[x] where x is the same number as that of Entry[x].\n",
    "\n",
    "These are respectively, \n",
    "- the result of the execution of the written code fragment: the code \"asks\" to display to the console the content of the string variable *message*, \n",
    "- the result of the state of the code fragment at the end of its execution with the list of the created variables, their type and their final value. In this example, the message variable of type String and value \"I understood\".\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La cellule de code ci-dessous reprend l'exemple à expérimenter. Vous devriez obtenir le même résultat dans la cellule précédente."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "I got it\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "\u001b[36mmessage\u001b[39m: \u001b[32mString\u001b[39m = \u001b[32m\"I got it\"\u001b[39m"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "val message = \"I got it\"\n",
    "println(message)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The capabilities\n",
    "\n",
    "We will limit ourselves here to the capabilities directly useful for the course:\n",
    "\n",
    "- You can **edit the contents of a code cell**, edit the code and execute it.\n",
    "- You can **add a code cell, write code to it** and execute it.\n",
    "- You can **modify a notebook** by modifying one or more code cells and/or adding one or more code cells.\n",
    "- You can **save a modified notebook locally on your computer**.\n",
    "- You can **upload a notebook from your computer to the notebook server**.\n",
    "\n",
    "The changes you make to the notebooks available on the server are kept **temporarily** for a certain period of time. If you want to be sure to keep your changes, it is safer to save a copy of the modified notebook on your computer.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Editing a code cell\n",
    "\n",
    "Place the cursor in the cell and write your code.\n",
    "If the cell already contains code, you can delete it, totally or partially.\n",
    "\n",
    "Once the **content of the cell has been modified**, the cell must be **executed so that its new content is compiled and executed**.\n",
    "\n",
    "Look at the code cell below. Try to understand the content and the result. Modify the code of the cell to get the message \"I'm going to get it\". To do this you only need to modify the second statement of the code fragment of the cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "I'm going \n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "\u001b[36ma\u001b[39m: \u001b[32mString\u001b[39m = \u001b[32m\"I'm going \"\u001b[39m\n",
       "\u001b[36mb\u001b[39m: \u001b[32mString\u001b[39m = \u001b[32m\"\"\u001b[39m"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "val a = \"I'm going \"\n",
    "val b = \"\"\n",
    "println(a + b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Add a code cell\n",
    "\n",
    "To add a code cell, you need to select the code or text cell before which you want to insert the new cell. To do this, simply click on the cell. It will be surrounded by a green frame. You must then click on the \"**+**\" button in the toolbar at the top of the notebook window.\n",
    "\n",
    "Then place the cursor in the cell, write the code fragment and execute it.\n",
    "\n",
    "Add a code cell after this text cell.\n",
    "\n",
    "Write the following code in it:\n",
    "\n",
    "*val a = 3<br>\n",
    "val b = 2<br>\n",
    "val c = a * b<br>\n",
    "println(c)*\n",
    "\n",
    "And execute it. Observe what happens."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Editing a notebook\n",
    "\n",
    "You can edit an existing code cell, add a code cell to an existing notebook. Modified notebooks are only kept temporarily on the server due to server congestion. If you have modified a notebook and you want to keep the changes, you must save it on your computer so that you can load and run it as explained below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Save a notebook\n",
    "\n",
    "To save a copy of a notebook to your computer, select the *\"Dowload as \"* option from the *\"File \"* menu and select the \"Notebook .ipynb\" format.\n",
    "\n",
    "Once you have added the code cell as requested above, save this modified notebook to your computer."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Loading a notebook\n",
    "\n",
    "To load a notebook from your computer onto the server to be able to run it, you must select the *\"Open \"* option from the *\"File \"* menu. A new window will open with all your notebook folders. Click on the *upload* button that you should see above the folder list on the right side of the window. **Select the notebook file and validate**. The file will be uploaded to the server and can be run.\n",
    "\n",
    "Upload the notebook that you saved just before."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# In case of a problem\n",
    "\n",
    "If you notice an **abnormal, strange behavior of the notebook**, you can **\"restart \"** it. To do this, select the option *\"Restart and run all \"* in the menu *\"Kernel \"* and confirm that you want to restart and run everything."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Scala",
   "language": "scala",
   "name": "scala"
  },
  "language_info": {
   "codemirror_mode": "text/x-scala",
   "file_extension": ".scala",
   "mimetype": "text/x-scala",
   "name": "scala",
   "nbconvert_exporter": "script",
   "version": "2.12.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
