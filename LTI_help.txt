Coursera :
 
Launch URL https://jupyter.unige.ch/hub/lti/launch
Consumer Key a267f82665f3939f8c33303fffdd8b13f1254337269d76bbb6cbbc41f027b59c
Secret 710dc9092ea2b9400e0bc36a4c1908c52129e83d432cdaa4bd0e91eccdba1565
next /hub/user-redirect/tree/Module_1/Leçon_6_Installation/01.06.03.ipynb

EdX :

URL LTI  https://jupyter.unige.ch/hub/lti/launch
identifiant LTI introProg-jupyter
Paramètres personnalisées next=/hub/user-redirect/tree/Module_1/Leçon_3_Instructions/01.03.02.ipynb
Pour tester le LTI il faut ouvrir la ressource à travers "Aperçu" sinon l'email n'est pas envoyer au serveur.
