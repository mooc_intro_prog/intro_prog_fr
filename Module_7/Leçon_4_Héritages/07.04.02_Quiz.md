# Héritage

Description comme dans le plan du cours.

Seuil de réussite: 80%
Temps : 5 min
Fonction du quiz: Formative

Q1 : Quelle clause utiliser pour définir une classe fille ?

Type de question:

- Réponse correcte unique

Réponses:

- extends Correct -> La clause extends permet d'étendre une autre classe.
- println Incorrect -> La méthode println permet d'afficher un message.
- private Incorrect -> La visibilité private permet de rendre un attribut privé.

Q2 : Dans les listes suivantes, quels sont les héritages corrects.

Type de question:

- Réponses correctes multiples

Réponses:

- Grand-mère -> Mère -> Fille Correct -> Cette définition d'héritage est correcte.
- Animaux -> Chat -> Chatton Correct -> Cette définition d'héritage est correcte.
- Salameche -> Pokémon -> Feu Incorrect -> Salameche devrait hériter de Pokémon et de la classe Feu.
- Europe -> Suisse -> Genève -> Correct Cette définition d'héritage est correcte.

Q3 : Comment invoquer une méthode d'une classe mère dans une classe fille ?

Type de question:

- Réponse correcte unique

Réponses:

- Dans la classe fille, faire un appel à la méthode de la classe mère avec super. Correct -> super permet d'appeler la méthode de la classe mère.
- Dans la classe fille, définir la méthode avec override. Incorrect -> En utilisant override, il faudra alors redéfinir la méthode en entier, il n'y aura aucun appel à la méthode de la classe mère.
- Dans la classe fille, faire un appel à la méthode de la classe mère avec equals.  Incorrect -> equals est une méthode classique, elle ne fait en rien appel à la méthode de la classe mère.
