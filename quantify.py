import io
import os
import nbformat
import re

def count_words_in_markdown(markdown):
    text = markdown

    # Comments
    text = re.sub(r'<!--(.*?)-->', '', text, flags=re.MULTILINE)
    # Tabs to spaces
    text = text.replace('\t', '    ')
    # More than 1 space to 4 spaces
    text = re.sub(r'[ ]{2,}', '    ', text)
    # Footnotes
    text = re.sub(r'^\[[^]]*\][^(].*', '', text, flags=re.MULTILINE)
    # Indented blocks of code
    text = re.sub(r'^( {4,}[^-*]).*', '', text, flags=re.MULTILINE)
    # Custom header IDs
    text = re.sub(r'{#.*}', '', text)
    # Replace newlines with spaces for uniform handling
    text = text.replace('\n', ' ')
    # Remove images
    text = re.sub(r'!\[[^\]]*\]\([^)]*\)', '', text)
    # Remove HTML tags
    text = re.sub(r'</?[^>]*>', '', text)
    # Remove special characters
    text = re.sub(r'[#*`~\-–^=<>+|/:]', '', text)
    # Remove footnote references
    text = re.sub(r'\[[0-9]*\]', '', text)
    # Remove enumerations
    text = re.sub(r'[0-9#]*\.', '', text)

    return len(text.split())

total_markdown = 0
total_heading = 0
total_code = 0


word_count_quiz = 0
word_count_other = 0

for root, dirs, files in os.walk("."):
    for file in files:
        if file.endswith(".ipynb") and not file.endswith("checkpoint.ipynb") :
            with io.open(os.path.join(root, file), 'r', encoding='utf-8') as f:
                nb = nbformat.read(f, 4)

            word_count_markdown = 0
            word_count_code = 0

            for cell in nb.cells:
                if cell.cell_type == "markdown":
                    word_count_markdown += len(cell['source'].replace('#', '').lstrip().split(' '))
                elif cell.cell_type == "code":
                    word_count_code += len(cell['source'].replace('#', '').lstrip().split(' '))
            total_markdown += word_count_markdown
            total_code += word_count_code
        if file.endswith(".md") and not file.endswith("-checkpoint.md")  and "Quiz" in file:
            with io.open(os.path.join(root, file), 'r', encoding='utf-8') as f:
                word_count_quiz += count_words_in_markdown(f.read())

        if file.endswith(".md") and not file.endswith("-checkpoint.md"):
            with io.open(os.path.join(root, file), 'r', encoding='utf-8') as f:
                word_count_other += count_words_in_markdown(f.read())
            
print("{} Words in notebooks'" .format(total_markdown+total_code))
print("{} Words in Quiz" .format(word_count_quiz))
print("{} Words in other" .format(word_count_other-word_count_quiz ))

