## Digression

In this quiz, you’ll be ask to look at some variable declarations and determine how their 
scope relates to various loops.

Passing grade: 80%
Duration: 5 min
Quiz type: Instructional

Q1: Why can't the following code be compiled?

``` scala
import scala.io.StdIn.readLine
do{

var money = readLine("How much money do you have?: ").toString
if(money == "0") println("You have no money.")
else println("You have some money.")

}while (money == "0")

println("You have "+ money +" euros")
```

Question type:
- Single correct answer

Answers:
- The scope of the variable money does not extend beyond the do loop; it is not accessible to the rest of the program.  Correct -> Yes, that’s the right answer.
- The variable money is not an Int.  Wrong -> Regardless of variable type, the problem is that the variable money is not declared outside the loop.
- The scope of the while loop prevents the code from being compiled.  Wrong -> While is a loop and loops do not have a scope. The variable money is what has a scope.

Q2: How should the following code be corrected? Select the code block that will make the program work correctly. 

```scala
import scala.io.StdIn.readLine
println("Weather simulator: rainy, sunny or cloudy")

do{
    var weather = readLine("What is the weather right now?: ").toString
    if (weather.contains("rainy")) println("The weather will be better later today.")
    if (weather.contains("sunny")) println("Rain is expected later today.")
    if (weather.contains("cloudy")) println("Rain or sunshine is expected later today.")
}while (weather.size == 0)
print("You entered " + weather)
```
Question type:

- Single correct answer 
Answers:

``` scala
 var weather = ""
do{ ...
```
Correct -> Yes, the variable weather has to be declared ahead of the do instruction block.

``` scala
do{ ...}
var weather = ""
```
Wrong -> Declaring the variable after the do block means the variable won’t be accessible to the elements contained in the do block.

``` scala
 
do{ ...}
while( var weather = "" 
  weather.size == 0   )
```

Wrong -> Declaring the variable inside the do while block means the variable will not be accessible to the elements contained in the do block.
