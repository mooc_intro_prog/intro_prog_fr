## for

This quiz is designed to measure your understanding of for loops.

Passing grade: 80%
Duration: 5 min
Quiz type: Instructional

Q1: Which of the following codes will display the multiplication table for the number 3, up to 10?
Question type:
- Single correct answer

Answers:

``` scala
for (i <- 0 to 10) println(i  3)
```

Correct -> Yes, that’s the right answer.

``` scala
for (i <- 0 to 3) println(i)
```
Wrong -> The variable i needs to be iterated from 0 to 10, not from 0 to 3. Also, i needs to be multiplied.

``` scala
for (0 to 10) println(i  3)
```
Wrong -> The variable to iterate needs to be in parentheses. This code will not compile.


Q2: Which of the following codes will display the alphabet in reverse order?
Question type:

- Single correct answer

Answers:

``` scala
var alpha= ""
for (x <- 'a' to 'z' ) alpha += x
val realpha = alpha.reverse
for (i <- realpha ) println(i)
```

- Correct -> Yes, you cannot use 'z' to 'a' to generate a reverse-order alphabet.

``` scala
for (x <- 'z' to 'a' ) println(x)
```

- Wrong -> This instruction returns nothing.

``` scala
var alpha= ""
for (x <- 'a' to 'z' ) alpha += x
val realpha = alpha.inverse
for (i <- realpha ) println(i)
```

- Wrong -> There is no method named inverse.
