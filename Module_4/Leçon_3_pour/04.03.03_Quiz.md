## for

Vous allez devoir répondre à un quiz sur le for.

Seuil de réussite: 80%
Temps : 5 min
Fonction du quiz: Formative

Q1 : Quel code permet d'afficher la table de multiplication de trois jusqu'à dix ?
Type de question:
- Réponse correcte unique

Réponses :

``` scala
for (i <- 0 to 10) println(i * 3)
```

Correct -> Bonne réponse

``` scala
for (i <- 0 to 3) println(i)
```
Incorrect -> Mauvaise réponse, l'itération de i doit se faire de 0 à 10 et non de 0 à 3, aussi il faut que la variable __i__ soit multipliée.

``` scala
for (0 to 10) println(i * 3)
```
Incorrect ->  Mauvaise réponse, la variable qui doit être itérée doit se trouver entre les parenthèses, ce code ne compilera pas.


Q2 : Quel code permet d'afficher l'alphabet à l'envers ?
Type de question:

- Réponse correcte unique

Réponses :

``` scala
var alpha= ""
for (x <- 'a' to 'z' ) alpha += x
val realpha = alpha.reverse
for (i <- realpha ) println(i)
```

- Correct -> Bonne réponse, il n'était pas possible d'utiliser 'z' to 'a' pour générer l'alphabet à l'envers.

``` scala
for (x <- 'z' to 'a' ) println(x)
```

- Incorrect -> Mauvaise réponse, cette instruction ne retourne rien.

``` scala
var alpha= ""
for (x <- 'a' to 'z' ) alpha += x
val realpha = alpha.inverse
for (i <- realpha ) println(i)
```

- Incorrect -> Mauvaise réponse, la méthode inverse n'existe pas.
