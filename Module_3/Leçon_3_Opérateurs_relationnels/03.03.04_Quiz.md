# Quel opérateur utiliser ?

Vous allez devoir répondre à un quiz reprenant l'ensemble des opérateurs relationnels.

Seuil de réussite: 80%
Temps : 3 min
Fonction du quiz: Formative


Q1 : Vous devez tester si une variable __a__ est comprise entre 10 et 15 . Quelle condition devez-vous utiliser ?

Type de question: 
- Réponse correcte unique 

Réponse :
- (a > 10) && (a < 15) -> Correct  && est l'opérateur ET logique en Scala.
- (a > 10) ||  (a > 15) -> Incorrect || est l'opérateur OU logique en Scala.
- (a < 10) ^  (a < 15)-> Incorrect ^ est l'opérateur OU exclusif logique en Scala.
- (a < 15) ! (a < 15) -> Incorrect ! est l'opérateur NON logique en Scala.

Q2 :  Vous devez tester si une variable __z__ est d'une valeur __A__ ou une valeur __B__.  Quelle condition devez-vous utiliser ?
Type de question: 
- Réponse correcte unique 
Réponse :

- z == A && z == B -> Incorrect  && est l'opérateur ET logique en Scala.
- z == A || z == B ->  Correct || est l'opérateur OU logique en Scala.
- z == A ^ z == B -> Incorrect ^ est l'opérateur OU exclusif logique en Scala.
- z == A ! z == B  -> Incorrect ! est l'opérateur NON logique en Scala.

Q3 : Vous voulez inverser (de true à false)la valeur une variable booléen gagné et l'assigné à la variable. Comment devez vous l'écrire ?

Type de question: 
- Réponse correcte unique 
Réponse :

- var perdu = &&gagné -> Incorrect  && est l'opérateur ET logique en Scala.
- var perdu = ||gagné -> Incorrect || est l'opérateur OU logique en Scala.
- var perdu = ^gagné -> Incorrect ^ est l'opérateur OU exclusif logique en Scala.
- var perdu = !gagné -> Correct ! est l'opérateur NON logique en Scala.
