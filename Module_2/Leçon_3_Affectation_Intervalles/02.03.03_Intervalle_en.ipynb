{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Types\n",
    "=========\n",
    "\n",
    "In this notebook, you’ll learn about the value intervals associated with\n",
    "each type.\n",
    "\n",
    "We’ve already learned about the intervals available for integers and\n",
    "real numbers:\n",
    "\n",
    "Integers\n",
    "--------\n",
    "\n",
    "-   Byte: encoded on 8 bits (1 bit for the sign, 7 bits for the\n",
    "    integer’s absolute value), -128 = 2 to the power -27 to 127 = 2 to\n",
    "    the power 27 -1 (corresponding to value 0)\n",
    "\n",
    "-   Short: encoded on 16 bits, -32768 to 32767\n",
    "\n",
    "-   Int: encoded on 32 bits, -2147483648 to 2147483647\n",
    "\n",
    "-   Long: encoded on 64 bits, -9223372036854775808 to\n",
    "    9223372036854775807\n",
    "\n",
    "Real numbers\n",
    "------------\n",
    "\n",
    "Float: 32 bits in single-precision floating point - ± 1.2 10-38 to ± 3.4\n",
    "10+38\n",
    "\n",
    "Double: 64 bits in double-precision floating point - ± 2.2 10-308 to ±\n",
    "1.8 10+308\n",
    "\n",
    "Depending on the information to be stored in a variable or constant,\n",
    "it’s important to select an appropriate type, one that offers an\n",
    "interval of values sufficient for manipulating the information.\n",
    "\n",
    "Here are some examples of situations and the choices that can be made:\n",
    "\n",
    "## Exemple 1\n",
    "\n",
    "Imagine you’re a developer working for a bank and all the account values\n",
    "are managed using **Int** variables. This would be a problem because\n",
    "you’d have no way of storing information about cents. A better choice\n",
    "would be a real number type, either **Float** or **Double**. Since it’s\n",
    "hard to know the highest possible value of a bank account, the best\n",
    "course of action would be to select whichever type offers the widest\n",
    "interval, namely **Double**. However, using **Double** introduces other\n",
    "issues.\n",
    "\n",
    "Monetary amounts that include cents will be represented using values\n",
    "with a precision of two places after the decimal, whereas the **Double**\n",
    "type can handle a lot more than that. This means that certain operations\n",
    "could lead to results with more than two numerals after the decimal\n",
    "point, which would require rounding the values. A solution based on\n",
    "integers is not totally impossible, if you consider the values as\n",
    "representing cents rather than dollars.\n",
    "\n",
    "This is not a good solution, however, because the value 100 represents\n",
    "just one francs, so the matter of which interval is best suited to\n",
    "represent the values quickly becomes an issue.\n",
    "\n",
    "As you can see, selecting a type is not always a simple task and there\n",
    "may not always be a perfect solution.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div class=\"jp-RenderedText\">\n",
       "<pre><code><span style=\"color: rgb(0, 187, 187)\"><span class=\"ansi-cyan-fg\">compte</span></span>: <span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">Double</span></span> = <style>@keyframes fadein { from { opacity: 0; } to { opacity: 1; } }</style><span style=\"animation: fadein 2s;\"><span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">1000.5</span></span></span></code></pre>\n",
       "</div>"
      ],
      "text/plain": [
       "\u001b[36mcompte\u001b[39m: \u001b[32mDouble\u001b[39m = \u001b[32m1000.5\u001b[39m"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "// The account values are in CHF\n",
    "// The variable is of type Double\n",
    "var compte :Double = 0.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The account is credited with 1000 CHF and 50 cents"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "compte = compte + 1000.5"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div class=\"jp-RenderedText\">\n",
       "<pre><code><span style=\"color: rgb(0, 187, 187)\"><span class=\"ansi-cyan-fg\">compte</span></span>: <span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">Long</span></span> = <span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">0L</span></span></code></pre>\n",
       "</div>"
      ],
      "text/plain": [
       "\u001b[36mcompte\u001b[39m: \u001b[32mLong\u001b[39m = \u001b[32m0L\u001b[39m"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "// The account values are in cents\n",
    "// The variable is of type Long\n",
    "var compte : Long = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The account is credited with 1000 CHF and 50 cents"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div class=\"jp-RenderedText\">\n",
       "<pre><code><span style=\"color: rgb(0, 187, 187)\"><span class=\"ansi-cyan-fg\">compte</span></span>: <span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">Int</span></span> = <span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">100050</span></span></code></pre>\n",
       "</div>"
      ],
      "text/plain": [
       "\u001b[36mcompte\u001b[39m: \u001b[32mInt\u001b[39m = \u001b[32m100050\u001b[39m"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "var compte = 100050"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Exemple 2\n",
    "\n",
    "\n",
    "Let’s say you’re charged with developing a program for medical testing\n",
    "laboratories. One of the things you are required to do is represent the\n",
    "patients’ ages. The values will be integers and definitely between 0 and\n",
    "150. This means the **Byte** type would be appropriate. Another integer\n",
    "type, such as **Int**, is not totally out of the question, but it will\n",
    "use up more memory space than necessary. Still for the same program, you\n",
    "now have to represent the patients’ temperatures.\n",
    "\n",
    "You will need to find out the unit used in Switzerland, presumably\n",
    "degrees Celsius, and the precision required: to one degree or to one-\n",
    "tenth of a degree. Based on these specifications, you’ll be able to\n",
    "select the right type, given that the interval of values will broadly be\n",
    "between 0 and 60 degrees Celsius.\n",
    "\n",
    "For a precision of one unit, the **Byte** type will work fine, but for a\n",
    "precision of one-tenth of a unit, **Float** is a better fit, unless you\n",
    "store the values in tenths-of-a-degree. So you see that selecting the\n",
    "type depends on the context and meaning of the value to be represented,\n",
    "as well as the unit and precision.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div class=\"jp-RenderedText\">\n",
       "<pre><code><span style=\"color: rgb(0, 187, 187)\"><span class=\"ansi-cyan-fg\">age</span></span>: <span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">Byte</span></span> = <style>@keyframes fadein { from { opacity: 0; } to { opacity: 1; } }</style><span style=\"animation: fadein 2s;\"><span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">50</span></span></span></code></pre>\n",
       "</div>"
      ],
      "text/plain": [
       "\u001b[36mage\u001b[39m: \u001b[32mByte\u001b[39m = \u001b[32m50\u001b[39m"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "// The age is declared of type Byte\n",
    "var age :Byte = 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "age = 50"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div class=\"jp-RenderedText\">\n",
       "<pre><code><span style=\"color: rgb(0, 187, 187)\"><span class=\"ansi-cyan-fg\">age</span></span>: <span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">Int</span></span> = <style>@keyframes fadein { from { opacity: 0; } to { opacity: 1; } }</style><span style=\"animation: fadein 2s;\"><span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">50</span></span></span></code></pre>\n",
       "</div>"
      ],
      "text/plain": [
       "\u001b[36mage\u001b[39m: \u001b[32mInt\u001b[39m = \u001b[32m50\u001b[39m"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "// Variant with age type Int\n",
    "var age = 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "age = 50"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div class=\"jp-RenderedText\">\n",
       "<pre><code><span style=\"color: rgb(0, 187, 187)\"><span class=\"ansi-cyan-fg\">temperature</span></span>: <span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">Byte</span></span> = <style>@keyframes fadein { from { opacity: 0; } to { opacity: 1; } }</style><span style=\"animation: fadein 2s;\"><span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">12</span></span></span></code></pre>\n",
       "</div>"
      ],
      "text/plain": [
       "\u001b[36mtemperature\u001b[39m: \u001b[32mByte\u001b[39m = \u001b[32m12\u001b[39m"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "// The temperature is declared of type Byte\n",
    "var temperature : Byte = 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "// It is only possible to store values to the nearest degree\n",
    "temperature = 12"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div class=\"jp-RenderedText\">\n",
       "<pre><code><span style=\"color: rgb(0, 187, 187)\"><span class=\"ansi-cyan-fg\">temperature</span></span>: <span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">Double</span></span> = <style>@keyframes fadein { from { opacity: 0; } to { opacity: 1; } }</style><span style=\"animation: fadein 2s;\"><span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">12.5</span></span></span></code></pre>\n",
       "</div>"
      ],
      "text/plain": [
       "\u001b[36mtemperature\u001b[39m: \u001b[32mDouble\u001b[39m = \u001b[32m12.5\u001b[39m"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "// Variant with temperature type Double\n",
    "var temperature = 0.0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "// It is possible to have values to the tenth of a degree\n",
    "temperature = 12.5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exemple 3\n",
    "\n",
    "Let's imagine that you are developing a program for an insurance company.\n",
    "\n",
    "In this context, you have to represent the category of a vehicle owned by an insured.\n",
    "The company provides you with its list of vehicle categories:\n",
    "- scooter\n",
    "- bicycle\n",
    "- scooter\n",
    "- motorcycle\n",
    "- car\n",
    "- truck\n",
    "\n",
    "The first idea would be to choose the String type for the variable representing the category.\n",
    "\n",
    "A lighter solution would be to use an integer type by defining a codification between the integer values and the categories. For example, the category scooter will be represented by the value 1, bike by the value 2..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div class=\"jp-RenderedText\">\n",
       "<pre><code><span style=\"color: rgb(0, 187, 187)\"><span class=\"ansi-cyan-fg\">categorie</span></span>: <span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">String</span></span> = <style>@keyframes fadein { from { opacity: 0; } to { opacity: 1; } }</style><span style=\"animation: fadein 2s;\"><span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">&quot;moto&quot;</span></span></span></code></pre>\n",
       "</div>"
      ],
      "text/plain": [
       "\u001b[36mcategorie\u001b[39m: \u001b[32mString\u001b[39m = \u001b[32m\"moto\"\u001b[39m"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "// The category is represented as String\n",
    "var categorie = \"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "// We assign the value of the motorcycle category to the variable\n",
    "categorie = \"moto\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div class=\"jp-RenderedText\">\n",
       "<pre><code><span style=\"color: rgb(0, 187, 187)\"><span class=\"ansi-cyan-fg\">categorie</span></span>: <span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">Int</span></span> = <style>@keyframes fadein { from { opacity: 0; } to { opacity: 1; } }</style><span style=\"animation: fadein 2s;\"><span style=\"color: rgb(0, 187, 0)\"><span class=\"ansi-green-fg\">4</span></span></span></code></pre>\n",
       "</div>"
      ],
      "text/plain": [
       "\u001b[36mcategorie\u001b[39m: \u001b[32mInt\u001b[39m = \u001b[32m4\u001b[39m"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "// Variant with type Int\n",
    "var categorie = 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "// We assign the value of the motorcycle category to the variable\n",
    "categorie = 4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## In conclusion\n",
    "\n",
    "We note that the choice of the type is linked to the context and the meaning of the value to be represented, as well as to the unit and precision. We note that the choice of a type is not direct and that there is not necessarily an ideal solution.\n",
    "\n",
    "It is important that the solution of the chosen type allows to manage the data with the necessary amplitude of values and precision. The range and precision can be larger than necessary, but not smaller."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Scala",
   "language": "scala",
   "name": "scala"
  },
  "language_info": {
   "codemirror_mode": "text/x-scala",
   "file_extension": ".scala",
   "mimetype": "text/x-scala",
   "name": "scala",
   "nbconvert_exporter": "script",
   "version": "2.12.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
