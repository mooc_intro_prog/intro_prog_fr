# Types 

This quiz is designed to measure your understanding of the 3 families of types.


Passing grade: 80%
Duration: 5 min
Quiz type: Instructional/Certifying

Q1: Which type is most appropriate if you want to save time-related information in seconds?


Question type: 
- Single correct answer 

Answers:
- Text  Wrong -> Text types will allow you to save this information, but they are not appropriate because manipulating the data will be difficult (adding, subtracting, etc.).
- Numeric  Correct-> Yes, this is the right type. 
- Logical  Wrong -> The Logical type does not allow you to save time-related information. 

Q2: What are the three families of types? 


Question type: 
- Single correct answer


Answers:
- Numeric, Text, Logical  Correct -> These are the 3 families of types.
- Numeric, Text, Analog  Wrong -> Analog is not one of the three families of types.
- Numeric, Text, Discrete  Wrong -> Discrete is not one of the three families of types.

Q3: Which of the following statements are true?

Question type: 
- Multiple correct answers 

Answers:

Numeric: integers, real numbers, 
Text: characters, character chains 
Logical: Booleans.

- The Numeric family of types comprises integers and real numbers. Correct -> This is a true statement.
- The Text family of types comprises characters and chains of characters. Correct -> This is a true statement.
- The Logical family of types comprises complex numbers. Wrong -> This is a false statement: logical types contain only Booleans.
