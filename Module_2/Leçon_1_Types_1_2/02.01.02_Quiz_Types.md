# Types 

Vous allez devoir répondre à un quiz qui reprend la notion des 3 familles de type.


Seuil de réussite: 80%
Temps : 5 min
Fonction du quiz: Formative/Certificative

Q1 : Quel type est le plus approprié pour sauvegarder une information de temps en seconde ?

Type de question:

- Réponse correcte unique

Réponses :

- Textuels Incorrect -> Les types Textuels permet de sauvegarder cette information, mais elle n'est pas approprié car la manipulation de l'information est difficile (addition,soustraction, ...) .
- Numériques Correct-> En effet, ce type est adéquate.
- Logique Incorrect->  Le type logique ne permet de sauvegarder une information de temps.

Q2 :Quelles sont les trois familles de type ?

Type de question:

- Réponses correctes unique

Réponses:

- Numériques, Textuels, Logique Correct -> Ce sont les 3 familles de type.
- Numériques, Logique, Analogue Incorrect -> Analogue n'est pas une des familles de type.
- Numériques, Logique, Discret  Incorrect -> Discret n'est pas une des familles de type.

Q3 : Laquelle de ces affirmations sont correctes ?

Type de question:

- Réponse correcte multiple

Réponses :

Numériques; entiers, réels,
Textuels: caractères, chaînes de caractères
Logiques: Booléens.

- La famille de type numérique contient des entiers et des réels. Correct -> Cette affirmation est juste.
- La famille de type textuels contient des caractères et chaînes de caractères. Correct-> Cette affirmation est juste.
- La famille de type logiques contient des nombres complexe. Incorrect -> Cette affirmation est fausse, la famille de type logiques contient des booléens.
