{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Defining a method’s inputs and outputs\n",
    "======================================\n",
    "\n",
    "In this notebook, you’ll learn to correctly \"parameterize\" a method\n",
    "(specify its formal input and output parameters).\n",
    "\n",
    "Simplicity\n",
    "----------\n",
    "\n",
    "When defining a method’s input parameters, you should always refer to\n",
    "the problem you are trying to solve. If any part of your code will be\n",
    "used repeatedly in the program, you may want to consider making it a\n",
    "method. Once a method is defined as such in your program, you can call\n",
    "it anywhere in your application.\n",
    "\n",
    "It’s essential that a method be simple. A method’s complexity should\n",
    "match the complexity of the task it is designed to perform. Let’s take a\n",
    "look at two different ways to code the method **cube**, which cubes (^3)\n",
    "the value of an integer.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "defined \u001b[32mfunction\u001b[39m \u001b[36mcubeanumber\u001b[39m\n",
       "defined \u001b[32mfunction\u001b[39m \u001b[36mcube\u001b[39m"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\n",
    "// Overly complex \n",
    "def cubeanumber(value:Double): Double={\n",
    "    val var1 = value\n",
    "    val var2 = value\n",
    "    val var3 = value\n",
    "    return var1 * var2 * var3\n",
    "}\n",
    "\n",
    "\n",
    "// Simple\n",
    "def cube(value:Double): Double={\n",
    "    value * value * value\n",
    "}\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\u001b[36mres1\u001b[39m: \u001b[32mDouble\u001b[39m = \u001b[32m1728.0\u001b[39m"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    " cubeanumber(12)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\u001b[36mres2\u001b[39m: \u001b[32mDouble\u001b[39m = \u001b[32m1728.0\u001b[39m"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "cube(12)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Both methods work and calculate the correct result. \n",
    "\n",
    "But it is not necessary to assign the same value to three different variables before doing the calculation. This is unnecessarily complicated and it is also inefficient, since it unnecessarily increases the number of instructions to be executed.\n",
    "\n",
    "These steps in the *cubeanumber* method unnecessarily complicate the code.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Explicitness\n",
    "============\n",
    "\n",
    "The name of the method, the names of the variables local to the method and of these formal input parameters must be explicit and indicate their role.\n",
    "\n",
    "By reading the method name and the formal input parameters, someone reading your code should be able to understand how to use it and how it works.\n",
    "\n",
    "Below we will see an example of poorly chosen variable names and explicit variable names.\n",
    "\n",
    "In this example we define a method that finds Pi from the area and diameter of a circle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "defined \u001b[32mfunction\u001b[39m \u001b[36mp\u001b[39m\n",
       "defined \u001b[32mfunction\u001b[39m \u001b[36mpi\u001b[39m"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\n",
    "// Not explicit: using each letter of the alphabet is not a good idea.\n",
    "def p(c:Double , d:Double): Double={\n",
    "    c/d\n",
    "}\n",
    "// Explicit\n",
    "def pi(circumference:Double , diameter:Double): Double={\n",
    "    circumference/diameter\n",
    "\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\u001b[36mres4\u001b[39m: \u001b[32mDouble\u001b[39m = \u001b[32m2.3653846153846154\u001b[39m"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "p(12.3,5.2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\u001b[36mres5\u001b[39m: \u001b[32mDouble\u001b[39m = \u001b[32m2.3653846153846154\u001b[39m"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "pi(12.3,5.2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also advisable to write comments for each method that indicate the purpose of the method and explain the role of each formal parameter;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "defined \u001b[32mfunction\u001b[39m \u001b[36mpi\u001b[39m"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "// pi method: calculation of the value of pi from a circle\n",
    "// circumference: value of the circumference of the circle\n",
    "// diameter: value of the diameter of the circle\n",
    "def pi(circumference:Double , diameter:Double): Double={\n",
    "    circumference/diameter\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Effectiveness\n",
    "=============\n",
    "\n",
    "Among several possible solutions for writing a method, it is recommended to use the most efficient one, i.e. the one that requires the fewest instructions to be executed.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "defined \u001b[32mfunction\u001b[39m \u001b[36mmettreaucube\u001b[39m\n",
       "defined \u001b[32mfunction\u001b[39m \u001b[36mcube\u001b[39m"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def mettreaucube(valeur:Double): Double={\n",
    "    val cube = Array(valeur,valeur,valeur)\n",
    "    var cumul = 1.0\n",
    "    for (i <- 0 until cube.size) cumul = cumul * cube(i)\n",
    "    return cumul\n",
    "}\n",
    "\n",
    "def cube(valeur:Double): Double={\n",
    "    valeur * valeur * valeur\n",
    "\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\u001b[36mres8\u001b[39m: \u001b[32mDouble\u001b[39m = \u001b[32m1728.0\u001b[39m"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "mettreaucube(12)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above *mettreaucube* method works, but it is less efficient than the *cube* method, as it requires many more instructions to run."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A final recommendation\n",
    "\n",
    "Finally, it is generally recommended to test a method separately before using it in a program.\n",
    "\n",
    "If the reuse of methods is recommended, you have to be careful with method calls in methods. It can become complicated to debug if there is a problem.\n",
    "\n",
    "Below is an example of a method that calculates the power of 4 of a value, declined in 3 different versions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "defined \u001b[32mfunction\u001b[39m \u001b[36mcarre\u001b[39m\n",
       "defined \u001b[32mfunction\u001b[39m \u001b[36mpuissance4_v1\u001b[39m\n",
       "defined \u001b[32mfunction\u001b[39m \u001b[36mpuissance4_v2\u001b[39m\n",
       "defined \u001b[32mfunction\u001b[39m \u001b[36mpuissance4_v3\u001b[39m"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def carre(valeur:Double): Double={\n",
    "    valeur * valeur \n",
    "}\n",
    "\n",
    "def puissance4_v1(valeur:Double): Double={\n",
    "    valeur * valeur * valeur * valeur\n",
    "}\n",
    "\n",
    "def puissance4_v2(valeur:Double): Double={\n",
    "    carre(valeur) * carre(valeur)\n",
    "}\n",
    "\n",
    "def puissance4_v3(valeur:Double): Double={\n",
    "    carre(carre(valeur))\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\u001b[36mres6\u001b[39m: \u001b[32mDouble\u001b[39m = \u001b[32m16.0\u001b[39m"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "puissance4_v1(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\u001b[36mres14\u001b[39m: \u001b[32mDouble\u001b[39m = \u001b[32m16.0\u001b[39m"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "puissance4_v2(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\u001b[36mres15\u001b[39m: \u001b[32mDouble\u001b[39m = \u001b[32m16.0\u001b[39m"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "puissance4_v3(2)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Scala",
   "language": "scala",
   "name": "scala"
  },
  "language_info": {
   "codemirror_mode": "text/x-scala",
   "file_extension": ".scala",
   "mimetype": "text/x-scala",
   "name": "scala",
   "nbconvert_exporter": "script",
   "version": "2.12.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
